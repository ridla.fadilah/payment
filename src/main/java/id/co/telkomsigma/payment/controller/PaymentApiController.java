package id.co.telkomsigma.payment.controller;

import java.util.Map;
import java.util.TreeMap;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:ridla.fadilah@gmail.com">Ridla Fadilah</a>
 */
@RestController
public class PaymentApiController {

    @GetMapping("/api/payment/{harga}")
    public Map<String, String> bayar(@PathVariable String harga){
        Map<String, String> hasil = new TreeMap<>();
        hasil.put("totalHarga", harga);
        hasil.put("status", "Payment Success");
        return hasil;
    }
    
}
